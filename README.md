# Kubernetes | Installation de Kubeadm et création d'un cluster

_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Ce laboratoire vise à présenter les étapes d'installation de l'outil kubeadm sur des instances ec2.

## Objectifs

Dans ce lab, nous allons :

- Provisionner 3 instances ec2 sur les quelles nous allons effectuer toutes nos opérations de configurations.

- Effectuer les configurations nécessaires sur ces instances afin d'y  installer **docker et containerd** sur l'ensemble des instances.

- Configurer les réglages de réseau avec Calico

- Gérer l'adhésion des nœuds workers au cluster principal.


En intégrant kubeadm dans notre flux de travail Kubernetes, nous simplifierons considérablement la gestion du cycle de vie de notre cluster, nous permettant ainsi de nous concentrer davantage sur le déploiement et la gestion de nos applications.


## 1. Définition des conceptes

### 1.1. C'est quoi kubeadm ?
kubeadm est un outil conçu par la communauté Kubernetes pour simplifier la mise en place et la gestion des composants fondamentaux d'un cluster Kubernetes de manière sécurisée et automatisée. Il est destiné à fournir une méthode standardisée pour la formation rapide de clusters Kubernetes.

### 1.2. Fonctionnalités de kubeadm

**Initialisation du Cluster :** kubeadm initialise les composants nécessaires pour faire fonctionner un cluster Kubernetes, y compris le serveur API, le Controller Manager et l'ordonnanceur (Scheduler). Cette étape comprend la configuration de la sécurité du cluster, la génération de certificats, et la configuration des composants de gestion du cluster.

**Gestion des Tokens et de la Sécurité :** kubeadm génère un token que les autres nœuds peuvent utiliser pour rejoindre le cluster de manière sécurisée. Il configure également les règles de base pour la communication sécurisée entre les nœuds du cluster.

**Configuration de Réseau :** Après l'initialisation, kubeadm attend que l'administrateur installe un add-on réseau avant que les nœuds ne soient prêts à exécuter des applications. Cela garantit que la communication inter-pod sur le cluster est bien établie.

**Ajout de Nœuds :** kubeadm fournit une commande simple pour ajouter des nœuds au cluster. Ceci est essentiel pour l'expansion du cluster.

Mise à niveau du Cluster : Avec kubeadm, la mise à niveau des composants du cluster vers de nouvelles versions est rationalisée et peut être exécutée avec des commandes simples, minimisant ainsi le risque d'erreurs humaines.

### 1.3. Pourquoi utiliser kubeadm ?

**Simplicité :** kubeadm réduit la complexité de mise en place d'un cluster Kubernetes, ce qui le rend accessible même pour ceux qui sont relativement nouveaux dans la technologie Kubernetes.

**Standardisation :** En utilisant kubeadm, vous suivez les meilleures pratiques et les configurations recommandées par la communauté Kubernetes, garantissant ainsi la fiabilité et la sécurité de votre cluster.

**Flexibilité :** kubeadm est conçu pour fonctionner bien avec les systèmes d'exploitation et les environnements d'hébergement les plus courants, ce qui en fait une solution versatile pour divers cas d'utilisation.


## 2. **Prérequis** : Liste des exigences matérielles et logicielles.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu 20.04 Focal Fossa LTS, grace au provider AWS, à partir delaquelle nous effectuerons toutes nos opérations.

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws) (recommandé)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)



## 1. Création d'un cluster

Nous allons provisionner 3 instances ec2 de type t3.medium ou avec des caractéristiques minimales (2 vCPU, 4Go de RAM)

### 1.1. Définition du nom d'hôte du "Control plan"

Nous allons commencer par renommer la première instance (principale)

````bash
sudo hostnamectl set-hostname k8s-control
````

une fois la commande entrée, on peut se déconnecter et se reconnecter de nouveau pour percevoir le changement.

>![alt text](img/image.png)
*k8s-Control*

### 1.2. Définition des noms d'hôtes

Nous allons ensuite renommé les 2 autres instaces respectivement en worker1 et worker2


````bash
sudo hostnamectl set-hostname k8s-worker1
````

````bash
sudo hostnamectl set-hostname k8s-worker2
````

>![alt text](img/image-1.png)


## 2. Mappage des serveurs

Depuis l'instance **k8s-control**, nous allons crée un mappage de l'ensemble des serveurs afin que la communication entre les différents serveurs puissent se faire via les noms d'hotes.

Nous allons éditer le fichier **hosts**

````bash
sudo nano /etc/hosts
````

dans ce fichier, nous allons rajouter les **ip privés** ainsi que les noms **utilisateurs** de l'ensemble des instances. 
il est absolument nécessaire d'utiliser des adresses privée à ce niveau, car les ip publiques peuvent changer après redemarrage des instances

````bash
172.31.23.252 k8s-control
172.31.30.26 k8s-worker1
172.31.30.220 k8s-worker2
````

>![alt text](img/image-2.png)


Nous allons copier ces informations, sauvegarder le fichier **hosts** et repéter la même opéération sur les deux autres serveur 

>![alt text](img/image-3.png)

Une fois ces opérations réalisées, on va se déconnecter et se reconnecter sur chacune des instances


## 3. Installation et configuration de Docker et Containerd

L'installation et la configuration de ces outils va se faire sur l'ensemble des instances

#### 3.1. Confiugration de  Docker Engine et containerd (valable pour toutes les instances)

Activation des modules du noyau 

````bash
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
````

>![alt text](img/image-4.png)

````bash
sudo modprobe overlay
````

````bash 
sudo modprobe br_netfilter
````

Les commandes **sudo modprobe** overlay et **sudo modprobe br_netfilter** jouent un rôle essentiel dans la configuration des environnements basés sur des conteneurs, notamment pour Docker et Kubernetes. Elles chargent respectivement les modules **overlay** et **br_netfilter** dans le noyau Linux.

**Module OverlayFS :** OverlayFS est un système de fichiers de type union qui permet de superposer deux systèmes de fichiers différents. En contexte de conteneurisation, il est souvent utilisé pour fournir le système de fichiers de conteneurs. Cela permet à chaque conteneur d'avoir son propre système de fichiers, tout en partageant des images de base entre conteneurs, économisant ainsi de l'espace disque et augmentant l'efficacité.

**Module br_netfilter :** Ce module permet le filtrage de trafic réseau au niveau des ponts Linux (bridges), essentiel pour la mise en place de réseaux de conteneurs.


### 3.2. Paramètres "sysctl" requis par la configuration et paramètres persistent après redémarrage. (valable pour toutes les instances)

Nous allons ensuite apporter des modifications relatives au réseau

````bash
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
````

>![alt text](img/image-5.png)


Ces commandes sont utilisées pour configurer des paramètres du noyau Linux via **"sysctl"**, qui sont nécessaires pour le bon fonctionnement de certaines applications, dans notre cas il s'agit de Kubernetes, lorsqu'elles utilisent Docker et containerd comme runtime de conteneurs.


### 3.3. Application du paramettre "sysctl" lors du redemarrage système (valable pour toutes les instances)

````bash
sudo sysctl --system
````

>![alt text](img/image-6.png)

Les nouvelles configurations sont rechargées

Nous pouvons poursuivre avec la configuration de docker.


### 3.3. Installation de Docker 

#### Configuration du repository de Docker  (valable pour toutes les instances)

````bash
sudo apt-get update && sudo apt-get install -y ca-certificates curl gnupg lsb-release apt-transport-https
````

#### Ajour de la clé GPG officiel Docker (valable pour toutes les instances)

````bash
sudo mkdir -m 0755 -p /etc/apt/keyrings
````

````bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
````

#### Configuration des dépots Docker sur la distribution linux utilisée (valable pour toutes les instances)

Ajout d'un dépôt Docker au fichier de sources de paquets APT sur un système Ubuntu

````bash
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
````

Cette commande configure le dépôt APT pour Docker sur une machine Ubuntu. Elle crée une ligne de source de dépôt, spécifiant l'architecture de la machine (obtenue via dpkg **--print-architecture**), le chemin du fichier de **clé GPG** pour la vérification des signatures (**signed-by=/etc/apt/keyrings/docker.gpg**), l'URL du dépôt Docker, et la version de distribution Ubuntu correspondante (obtenue via **lsb_release -cs**).


#### Mise à jour des index de packets apt (valable pour toutes les instances)

````bash
sudo apt-get update
````

#### Installation de Docker, containerd, et Docker Compose (valable pour toutes les instances)

````bash
VERSION_STRING=5:23.0.1-1~ubuntu.20.04~focal
````

````bash
sudo apt-get install -y docker-ce=$VERSION_STRING docker-ce-cli=$VERSION_STRING containerd.io docker-buildx-plugin docker-compose-plugin
````

>![alt text](img/image-7.png)


#### Ajout de l'utilisateur courant au au groupe docker (valable pour toutes les instances)

````bash
sudo usermod -aG docker $USER
````

Une fois cette commande entrée, se déconnecter et se reconnecter, il est alors possible d'exécuter les commandes docker passer le "sudo" au préalable


#### Mise en commentaire de 'disabled_plugins' dans le fichier **config.toml** (valable pour toutes les instances)

````bash
sudo sed -i 's/disabled_plugins/#disabled_plugins/' /etc/containerd/config.toml
````
Cette commande est utilisée pour commenter la ligne **disabled_plugins** dans le fichier de configuration de containerd. Cette opération permet d'activer les plugins qui étaient désactivés, car la suppression du préfixe "disabled_" modifie la configuration pour ne plus désactiver ces plugins. Cela est nécessaire pour assurer que certaines fonctionnalités ou dépendances de containerd soient disponibles et fonctionnelles, notamment lors de l'exécution de conteneurs Docker.


#### Redemarrage de containerd (valable pour toutes les instances)

````bash
sudo systemctl restart containerd
````

#### Désactivation de **swap** (valable pour toutes les instances)

````bash
sudo swapoff -a
````

## 4. Installation de kubeadm, kubelet, and kubectl (valable pour toutes les instances)

### 4.1. Installation

````bash
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.27/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
````
Cette opération nous permet de télécharger la clé publique de signature de paquet depuis une URL spécifique, destinée aux packages Kubernetes. Cette clé est nécessaire pour authentifier et sécuriser les téléchargements de packages Kubernetes


````bash
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.27/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
````

Ajout d'une source de dépôt pour Kubernetes dans les sources APT du système. Cette opération permet à APT de vérifier l'authenticité des paquets téléchargés de ce dépôt.


````bash
sudo apt-get update && sudo apt-get install -y kubelet=1.27.0-* kubeadm=1.27.0-* kubectl=1.27.0-*
````

````bash
sudo apt-mark hold kubelet kubeadm kubectl
````

Cette commande empêche la mise à jour automatique des trois paquets, ce qui nous donne un control total sur la mise à jour de kubernetes

>![alt text](img/image-14.png)
*Installation de kubelet, kubeadm et kubectl*

### 4.1. Initialisatoin du cluster et configuration des l'accès kubectl.

````bash
sudo kubeadm init --pod-network-cidr 192.168.0.0/16 --kubernetes-version 1.27.0
````

Cette commande est utilisée pour initialiser un cluster Kubernetes. Cette initialisation est la première étape pour configurer un cluster Kubernetes.

>![alt text](img/image-9.png)


Les commandes suivantes configurent l'accès à notre cluster Kubernetes pour notre utilisateur courant.

````bash
mkdir -p $HOME/.kube
````

````bash
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
````
````bash
sudo chown $(id -u):$(id -g) $HOME/.kube/config
````

## 5. Vérification du bon fonctionnement du cluster

````bash
kubectl get nodes
````

>![alt text](img/image-10.png)


## 6. Install the Calico network add-on (Control plan)

**Calico** est un système de réseau et de sécurité pour conteneurs, sur un cluster Kubernetes. 

Via la commande ci-dessous, Kubernetes télécharge et applique la configuration définie dans le fichier YAML qui sera téléchargé, installant ensuite **Calico** pour gérer le réseau, les politiques de sécurité, et d'autres fonctionnalités de réseau au sein de notre cluster.

````bash
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.25.0/manifests/calico.yaml
````
Ce manifest va crée un ensemble d'objet utile pour notre réseau grace à l'outil Calico.

>![alt text](img/image-8.png)

A présent joignons les workers au control plan

## 7. Génération de la commande de communication avec le "Control plan" (Control plan)

````bash
kubeadm token create --print-join-command
````

>![alt text](img/image-11.png)

Cette opération nous permet de générer un nouveau token d'authentification dans notre cluster Kubernetes tout en affichant la commande complète à utiliser sur les autres nœuds qui permettra de rejoindre le cluster. 


````bash
sudo kubeadm join 172.31.23.252:6443 --token 6a91sy.5al82ueyuhtep9a4 --discovery-token-ca-cert-hash sha256:f46933aa3d09c2b58f649e2ff4bc8fab2ce0123bf912efe27ad803725a0cd50a
````

Nous allons exécuter cette commande en sudo sur les workers

>![alt text](img/image-12.png)


NB : vérifier les groupes de sécurités au niveau des workers sur AWS afin de s'assurer que la communication est bien possible sur le port **6443**

### Vérification des nodes

````bash
kubectl get nodes
````
>![alt text](img/image-13.png)


## Documentation 

### [Installer Docker sur ubuntu](https://docs.docker.com/engine/install/ubuntu/)

### [Installer kubeadm sur ubuntu](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

### [Creating a Single Control-Plane Cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

### 